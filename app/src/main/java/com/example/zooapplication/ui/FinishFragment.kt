package com.example.zooapplication.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapplication.R
import com.example.zooapplication.ZooActivity
import com.example.zooapplication.databinding.FragmentFinishBinding

class FinishFragment: Fragment() {

    private var _binding: FragmentFinishBinding? = null
    private val binding get() = _binding!!
    private val zooName = "Elden Zoo"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFinishBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // SafeArgs Method
        val backBtn = FinishFragmentDirections.actionFinishFragmentToGettingStartedFragment()
        val zooBtn = FinishFragmentDirections.actionFinishFragmentToZooActivity()

        binding.btnBack.setOnClickListener {
            findNavController().navigate(backBtn)
        }
        // Button to go to ZooActivity
        binding.btnFinish.setOnClickListener {
            findNavController().navigate(zooBtn)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}