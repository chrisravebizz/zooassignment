package com.example.zooapplication.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapplication.R
import com.example.zooapplication.databinding.FragmentGettingstartedBinding

class GettingStartedFragment : Fragment() {

    private var _binding: FragmentGettingstartedBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGettingstartedBinding.inflate(inflater, container, false)
        .also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // SafeArgs Method
        val backDir = GettingStartedFragmentDirections.actionGettingStartedFragmentToHelloFragment()
        val nextDir = GettingStartedFragmentDirections.actionGettingStartedFragmentToFinishFragment()

        binding.btnBack.setOnClickListener {
            findNavController().navigate(backDir)
        }
        binding.btnNext.setOnClickListener {
            findNavController().navigate(nextDir)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}